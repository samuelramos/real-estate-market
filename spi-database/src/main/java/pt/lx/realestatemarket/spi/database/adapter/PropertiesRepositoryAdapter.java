package pt.lx.realestatemarket.spi.database.adapter;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pt.lx.realestatemarket.shared.spi.dto.PropertyDto;
import pt.lx.realestatemarket.spi.database.adapter.mapper.PropertyMapper;
import pt.lx.realestatemarket.spi.database.model.Property;
import pt.lx.realestatemarket.spi.database.repository.IPropertyRepository;
import pt.lx.realestatemarket.core.spi.IPropertiesRepositoryPort;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Component
@Slf4j
public class PropertiesRepositoryAdapter implements IPropertiesRepositoryPort {

    private IPropertyRepository propertyRepository;

    private PropertyMapper propertyMapper;

    public PropertiesRepositoryAdapter(IPropertyRepository propertyRepository) {
        this.propertyRepository = propertyRepository;
        this.propertyMapper = new PropertyMapper();
    }


    @Override
    public List<PropertyDto> getAll() {
        log.info("[PropertiesRepositoryAdapter] getAll() called");
        List <Property> result = propertyRepository.findAll();
        return result.stream().map(p -> propertyMapper.transform(p)).collect(Collectors.toList());
    }

    @Override
    public Optional<PropertyDto> getById(Long id) {
        log.info("[PropertiesRepositoryAdapter] getById() called");
        Optional <Property> result = propertyRepository.findById(id);
        return result.map(p -> propertyMapper.transform(p));
    }

    @Override
    public PropertyDto save(PropertyDto property) {
        log.info("[PropertiesRepositoryAdapter] save() called");
        Property result = propertyRepository.save(propertyMapper.transform(property));
        return propertyMapper.transform(result);
    }

    @Override
    public void delete(Long id) {
        propertyRepository.deleteById(id);
    }
}
