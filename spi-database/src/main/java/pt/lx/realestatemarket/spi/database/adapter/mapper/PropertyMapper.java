package pt.lx.realestatemarket.spi.database.adapter.mapper;

import lombok.NoArgsConstructor;
import pt.lx.realestatemarket.shared.spi.dto.PropertyDto;
import pt.lx.realestatemarket.spi.database.model.Property;

@NoArgsConstructor
public class PropertyMapper {

    public PropertyDto transform(Property property) {
        return new PropertyDto(property.getId(), property.getUrl(), property.getName());
    }

    public Property transform(PropertyDto propertyDto) {
        return new Property(propertyDto.getId(), propertyDto.getUrl(), propertyDto.getName());
    }

}
