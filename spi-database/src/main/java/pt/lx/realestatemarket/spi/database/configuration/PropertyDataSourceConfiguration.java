package pt.lx.realestatemarket.spi.database.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import pt.lx.realestatemarket.spi.database.model.Property;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;



@Configuration
@PropertySource("classpath:repository-${spring.profiles.active}.properties")
@EnableJpaRepositories(basePackages = "pt.lx.realestatemarket.spi.database.repository",
                        entityManagerFactoryRef = "propertyEntityManagerFactory",
                        transactionManagerRef= "propertyTransactionManager")
@EnableTransactionManagement
@Slf4j
public class PropertyDataSourceConfiguration {

    @Autowired
    Environment env;

    @Bean(name = "propertyDataSource")
    @ConfigurationProperties(prefix="app.datasource.property")
    public DataSource propertyDataSource() {

        log.info("[PropertyDataSourceConfiguration] propertyDataSource() OK");
        log.info("dataSource =  "+env.getProperty("app.datasource.property.jdbcUrl"));

        return DataSourceBuilder.create().build();
    }

    @Bean(name = "propertyEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean propertyEntityManagerFactory(
            EntityManagerFactoryBuilder builder,
            @Qualifier("propertyDataSource") DataSource dataSource) {

        log.info("[PropertyDataSourceConfiguration] propertyEntityManagerFactory() OK");

        return builder
                .dataSource(dataSource)
                .packages(Property.class)
                .persistenceUnit("property")
                .build();
    }


    @Bean
    public PlatformTransactionManager propertyTransactionManager(
            @Qualifier("propertyEntityManagerFactory") EntityManagerFactory propertyEntityManagerFactory) {

        log.info("[PropertyDataSourceConfiguration] propertyTransactionManager() OK");

        return new JpaTransactionManager(propertyEntityManagerFactory);
    }



}