package pt.lx.realestatemarket.spi.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pt.lx.realestatemarket.spi.database.model.Property;

@Repository
public interface IPropertyRepository extends JpaRepository <Property, Long> {

    @Query("select p from Property p where p.name = :name")
    Property findByName(@Param("name") String name);

}