package pt.lx.realestatemarket.spi.database.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import pt.lx.realestatemarket.spi.database.model.Property;
import pt.lx.realestatemarket.spi.database.repository.IPropertyRepository;

@DataJpaTest
@ActiveProfiles("test")
public class AddPropertyV1IT {

    @Autowired
    private IPropertyRepository propertyRepository;

    @Test
    public void scenarioA() {

        Property property = new Property(Long.parseLong("1"), "http://property-1/", "property 1" );
        Property result = propertyRepository.save(property);

        Assertions.assertEquals(result.getId(),property.getId());
        Assertions.assertEquals(result.getUrl(),property.getUrl());
        Assertions.assertEquals(result.getName(),property.getName());

    }

}
