package pt.lx.realestatemarket.spi.database.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import pt.lx.realestatemarket.shared.spi.dto.PropertyDto;
import pt.lx.realestatemarket.core.spi.IPropertiesRepositoryPort;

@SpringBootTest
@ActiveProfiles("test")
public class AddPropertyV2IT {

    @Autowired
    private IPropertiesRepositoryPort propertiesRepositoryAdapter;

    @Test
    public void scenarioA() {

        PropertyDto property = new PropertyDto(Long.parseLong("1"), "http://property-1/", "property 1" );

        PropertyDto result = propertiesRepositoryAdapter.save(property);

        Assertions.assertEquals(result.getId(),property.getId());
        Assertions.assertEquals(result.getUrl(),property.getUrl());
        Assertions.assertEquals(result.getName(),property.getName());

    }

}
