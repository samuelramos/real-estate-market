package pt.lx.realestatemarket.spi.database.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import pt.lx.realestatemarket.shared.spi.dto.PropertyDto;
import pt.lx.realestatemarket.spi.database.adapter.PropertiesRepositoryAdapter;
import pt.lx.realestatemarket.spi.database.model.Property;
import pt.lx.realestatemarket.core.spi.IPropertiesRepositoryPort;
import pt.lx.realestatemarket.spi.database.repository.IPropertyRepository;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {IPropertyRepository.class, IPropertiesRepositoryPort.class, PropertiesRepositoryAdapter.class})
@ActiveProfiles("test")
public class AddPropertyV3IT {

    @MockBean
    private IPropertyRepository propertyRepository;

    @Autowired
    private IPropertiesRepositoryPort propertiesRepositoryAdapter;

    @Test
    public void scenarioA() {

        PropertyDto property = new PropertyDto(Long.parseLong("1"), "http://property-1/", "property 1" );

        when(propertyRepository.save(any(Property.class))).then(returnsFirstArg());

        PropertyDto result = propertiesRepositoryAdapter.save(property);

        Assertions.assertEquals(result.getId(),property.getId());
        Assertions.assertEquals(result.getUrl(),property.getUrl());
        Assertions.assertEquals(result.getName(),property.getName());

    }

}
