package pt.lx.realestatemarket.spi.database.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.context.ActiveProfiles;
import pt.lx.realestatemarket.spi.database.adapter.PropertiesRepositoryAdapter;
import pt.lx.realestatemarket.shared.spi.dto.PropertyDto;
import pt.lx.realestatemarket.spi.database.model.Property;
import pt.lx.realestatemarket.core.spi.IPropertiesRepositoryPort;
import pt.lx.realestatemarket.spi.database.repository.IPropertyRepository;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
public class AddPropertyV1Test {

    private IPropertyRepository propertyRepository = Mockito.mock(IPropertyRepository.class);
    private IPropertiesRepositoryPort propertiesRepositoryAdapter;

    @BeforeEach
    void initMocks() {
        propertiesRepositoryAdapter = new PropertiesRepositoryAdapter(propertyRepository);
    }

    @Test
    public void scenarioA() {

        PropertyDto property = new PropertyDto(Long.parseLong("1"), "http://property-1/", "property 1" );

        when(propertyRepository.save(any(Property.class))).then(returnsFirstArg());

        PropertyDto result = propertiesRepositoryAdapter.save(property);

        Assertions.assertEquals(result.getId(),property.getId());
        Assertions.assertEquals(result.getUrl(),property.getUrl());
        Assertions.assertEquals(result.getName(),property.getName());

    }

}
