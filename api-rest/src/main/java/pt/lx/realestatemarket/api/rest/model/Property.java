package pt.lx.realestatemarket.api.rest.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Property {

    private Long id;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String url;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String name;

    @JsonCreator
    public Property(@JsonProperty("id") Long id, @JsonProperty("url")  String url, @JsonProperty("name") final String name) {
        this.id = id;
        this.url = url;
        this.name = name;
    }

}
