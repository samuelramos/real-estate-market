package pt.lx.realestatemarket.api.rest.configuration;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityScheme;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class OpenAPIConfiguration {

    @Bean
    public OpenAPI customOpenAPI() {
        log.info("[OpenAPIConfiguration] customOpenAPI() OK");
        return new OpenAPI().components(new Components()
                                                .addSecuritySchemes("basicScheme", new SecurityScheme().type(SecurityScheme.Type.HTTP).scheme("basic")))
                                                .info(new Info()
                                                        .title("Real Estate Market API")
                                                        .version("1")
                                                        .license(new License().name("Apache 2.0").url("http://springdoc.org")));
    }


}
