package pt.lx.realestatemarket.api.rest.controllers.mapper;

import lombok.NoArgsConstructor;
import pt.lx.realestatemarket.shared.api.dto.PropertyDto;
import pt.lx.realestatemarket.api.rest.model.Property;

@NoArgsConstructor
public class PropertyMapper {

    public PropertyDto transform(Property property) {
        return new PropertyDto(property.getId(), property.getUrl(), property.getName());
    }

    public Property transform(PropertyDto propertyDto) {
        return new Property(propertyDto.getId(), propertyDto.getUrl(), propertyDto.getName());
    }

}
