package pt.lx.realestatemarket.api.rest.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.lx.realestatemarket.api.rest.controllers.mapper.PropertyMapper;
import pt.lx.realestatemarket.api.rest.model.Property;
import pt.lx.realestatemarket.core.api.IPropertyCommandsPort;
import pt.lx.realestatemarket.core.api.IPropertyQueriesPort;
import pt.lx.realestatemarket.shared.api.dto.PropertyDto;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Tag(name = "properties", description = "Access to properties from Real Estate Market")
@RestController
@RequestMapping("api/v1/properties")
@Slf4j
public class PropertiesController {

    private IPropertyQueriesPort propertyQueriesPort;

    private IPropertyCommandsPort propertyCommandsPort;

    private PropertyMapper propertyMapper;

    public PropertiesController(IPropertyQueriesPort propertyQueriesPort, IPropertyCommandsPort propertyCommandsPort) {
        this.propertyQueriesPort = propertyQueriesPort;
        this.propertyCommandsPort = propertyCommandsPort;
        this.propertyMapper = new PropertyMapper();
    }


    @Operation(description = "Saves a property in the repository",
            responses = {
                @ApiResponse(responseCode = "201", description = "Created", content = @Content(schema = @Schema(implementation = Property.class))),
                @ApiResponse(responseCode = "401", description = "Not Authorized", content = @Content(schema = @Schema(implementation = Void.class))),
                @ApiResponse(responseCode = "403", description = "Forbidden", content = @Content(schema = @Schema(implementation = Void.class)))})
    @PostMapping(value = "/")
    ResponseEntity <Property> saveProperty(@RequestBody Property property) {
        log.info("[PropertiesAdapter] saveProperty() called");
        Long propertyId = propertyCommandsPort.saveProperty(propertyMapper.transform(property));
        return new ResponseEntity <Property> (Property.builder().id(propertyId).build(), HttpStatus.CREATED);
    }

    @Operation(description = "Delete a specific property from the repository",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Success", content = @Content(schema = @Schema(implementation = Void.class))),
                    @ApiResponse(responseCode = "401", description = "Not Authorized", content = @Content(schema = @Schema(implementation = Void.class))),
                    @ApiResponse(responseCode = "403", description = "Forbidden", content = @Content(schema = @Schema(implementation = Void.class))),
                    @ApiResponse(responseCode = "404", description = "Not Found", content = @Content(schema = @Schema(implementation = Void.class)))})
    @DeleteMapping(value = "/{propertyId}")
    ResponseEntity<Void> deleteProperty(@Parameter(description = "Property Id") @PathVariable Long propertyId) {
        log.info("[PropertiesController] deleteProperty() called");
        propertyCommandsPort.deleteProperty(propertyId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @Operation(description = "Get all the properties from the repository",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Success", content = @Content(array = @ArraySchema(schema = @Schema(implementation = Property.class)))),
                    @ApiResponse(responseCode = "401", description = "Forbidden", content = @Content(schema = @Schema(implementation = Void.class))),
                    @ApiResponse(responseCode = "403", description = "Not Found", content = @Content(schema = @Schema(implementation = Void.class)))})
    @GetMapping(value = "/")
    ResponseEntity <List <Property>> getProperties() {
        log.info("[PropertiesController] getProperties() called");
        List <PropertyDto> properties = propertyQueriesPort.getAllProperties();
        List <Property> result = properties.stream().map(p -> propertyMapper.transform(p)).collect(Collectors.toList());
        return new ResponseEntity <List <Property>> (result, HttpStatus.OK);
    }

    @Operation(description = "Get a specific property from the repository",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Success", content = @Content(schema = @Schema(implementation = Property.class))),
                    @ApiResponse(responseCode = "401", description = "Not Authorized", content = @Content(schema = @Schema(implementation = Void.class))),
                    @ApiResponse(responseCode = "403", description = "Forbidden", content = @Content(schema = @Schema(implementation = Void.class))),
                    @ApiResponse(responseCode = "404", description = "Not Found", content = @Content(schema = @Schema(implementation = Void.class)))})
    @GetMapping(value = "/{propertyId}")
    ResponseEntity <Optional <Property>> getProperty(@Parameter(description = "Property Id") @PathVariable Long propertyId) {
        log.info("[PropertiesController] getProperty() called");
        Optional <PropertyDto> property = propertyQueriesPort.getPropertyById(propertyId);
        Optional <Property> result = property.map(p -> propertyMapper.transform(p));
        return new ResponseEntity <Optional <Property>> (result, HttpStatus.OK);
    }

}
