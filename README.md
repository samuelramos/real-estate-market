# Real Estate Market

### Story 1
As a user
I want to save properties advertisements
So that I can enrich my real estate market repository

### Story 2
As a user
I want to query all the properties in my real estate market repository
So that I can access properties details

[...]

## Pre-requirements

a. JRE >= 8 \

```
instuctions JRE setup
```

b. Maven \

```
instuctions Maven setup
```

c. Docker

```
instuctions Docker setup
```

## Test the solution

### Build and test the application

```
./mvnw clean package -Dspring.profiles.active=test
```

#### From the parent module

Build a specific module and any of it's dependencies

```
./mvnw clean test -pl <target-child-module> -am -Dspring.profiles.active=test
```

#### From the child module

First, build and install the parent module in the repository

```
./mvn clean install -N
```

Second, build and install the dependency in the repository

```
cd <dependency-child-module>
./mvn clean install
```

Then, build and test the target module

```
cd <target-child-module>
./mvn clean test -Dspring.profiles.active=test
```

## Run the solution

### 1. Using the JRE of your local machine

#### Pre-requirements

a) Database 

Tto be simpler, you can use Docker, and start a database container

```
docker run -d --name real-estate-market-db --hostname real-estate-market-db -p 5555:3306 -e MYSQL_DATABASE=real-estate-market-db -e MYSQL_ROOT_PASSWORD=passw0rd mysql:8
```

#### Build and execute the application

```
./mvnw clean package -Dspring.profiles.active=test && java -Dspring.profiles.active=local -jar application/target/application-1.0-SNAPSHOT.jar
```

To skip tests use:

```
./mvnw clean package -DskipTests && java -Dspring.profiles.active=local -jar application/target/application-1.0-SNAPSHOT.jar
```

### 2. Using Docker

Build and test the solution

```
./mvnw clean package -Dspring.profiles.active=test 
```

Build docker image

```
docker build -t real-estate-market-app .
```

Create a docker network

```
docker network create real-estate-market-network
```

Start a database container

```
docker run --name real-estate-market-db --net real-estate-market-network --hostname real-estate-market-db -p 4403:3306 -e MYSQL_DATABASE=real-estate-market-db -e MYSQL_ROOT_PASSWORD=passw0rd mysql:8
```

Start the application container

```
docker run -e spring.profiles.active=qa --name real-estate-market-app --net real-estate-market-network -p 8080:8080 -t real-estate-market-app 
```

or, to keep the process running in background

```
docker run -e spring.profiles.active=qa --name real-estate-market-app --net real-estate-market-network -p 8080:8080 -t real-estate-market-app -d
```

#### 2.1. Using docker-compose

Build and test the solution

```
./mvnw clean package -Dspring.profiles.active=test 
```

Build the docker images

```
docker-compose build
```

Start the docker containers

```
docker-compose up 
```

or, to keep the process running in background

```
docker-compose up -d 
```

Stop and destroy the docker containers

```
docker-compose stop
docker-compose rm
```

## Validate the solution

### API

Endpoint:

http://localhost:8080/real-estate-market-local/api/v1/properties/

Swagger:

http://localhost:8080/real-estate-market-local/swagger-ui.html

http://localhost:8080/real-estate-market-local/swagger-ui/index.html?configUrl=/real-estate-market-local/v3/api-docs/swagger-config

api-docs:

http://localhost:8080/real-estate-market-local/v3/api-docs

http://localhost:8080/real-estate-market-local/v3/api-docs.yaml

## Monitoring


$ docker run -d --name=prometheus -p 9090:9090 -v ${PWD}/prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus --config.file=/etc/prometheus/prometheus.yml 

$ docker run -d --name=grafana -p 3000:3000 grafana/grafana 




## FAQ

### How to setup Maven wrapper

Use the following command

```
mvn -N io.takari:maven:wrapper
```

## References

Documenting Spring Boot REST API with SpringDoc + OpenAPI 3

From <https://www.dariawan.com/tutorials/spring/documenting-spring-boot-rest-api-springdoc-openapi-3/>

springdoc-openapi

From <https://springdoc.github.io/springdoc-openapi-demos/>
From <https://github.com/springdoc/springdoc-openapi>
From <https://github.com/springdoc/springdoc-openapi-demos>

HTTP Status Codes

From <https://httpstatuses.com/>

HikariCP

From <https://github.com/brettwooldridge/HikariCP#configuration-knobs-baby>

Pipeline Syntax

From <https://jenkins.io/doc/book/pipeline/syntax/>




Spring Boot Actuator metrics monitoring with Prometheus and Grafana

From <https://www.callicoder.com/spring-boot-actuator-metrics-monitoring-dashboard-prometheus-grafana/>

https://stackabuse.com/monitoring-spring-boot-apps-with-micrometer-prometheus-and-grafana/

From <https://stackabuse.com/monitoring-spring-boot-apps-with-micrometer-prometheus-and-grafana/>
