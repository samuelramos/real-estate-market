package pt.lx.realestatemarket.shared.api.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class PropertyDto {

    private Long id;
    private String url;
    private String name;


    public PropertyDto(Long id, String url, final String name) {
        this.id = id;
        this.url = url;
        this.name = name;
    }

}
