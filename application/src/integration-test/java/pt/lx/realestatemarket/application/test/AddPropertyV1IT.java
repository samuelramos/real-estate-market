package pt.lx.realestatemarket.application.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import pt.lx.realestatemarket.core.spi.IPropertiesRepositoryPort;
import pt.lx.realestatemarket.core.api.IPropertyCommandsPort;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
public class AddPropertyV1IT {

    @MockBean
    private IPropertiesRepositoryPort propertiesRepositoryPort;

    @Autowired
    private IPropertyCommandsPort propertiesServicePort;

    @Test
    public void scenarioA(){

        pt.lx.realestatemarket.shared.api.dto.PropertyDto property = new pt.lx.realestatemarket.shared.api.dto.PropertyDto(Long.parseLong("1"), "http://teste-1/", "test-1");
        when(propertiesRepositoryPort.save(any(pt.lx.realestatemarket.shared.spi.dto.PropertyDto.class))).then(returnsFirstArg());
        Long propertyId = propertiesServicePort.saveProperty(property);
        Assertions.assertEquals(property.getId(),propertyId);

    }

}
