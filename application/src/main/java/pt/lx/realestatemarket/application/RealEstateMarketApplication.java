package pt.lx.realestatemarket.application;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan(basePackages = "pt.lx.realestatemarket")
@Slf4j
public class RealEstateMarketApplication {

    public static void main(String[] args) {
        SpringApplication.run(RealEstateMarketApplication.class, args);
        log.info("[RealEstateMarketApplication] OK");
    }

}