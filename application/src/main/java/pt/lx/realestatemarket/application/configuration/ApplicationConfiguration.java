package pt.lx.realestatemarket.application.configuration;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pt.lx.realestatemarket.core.api.IPropertyCommandsPort;
import pt.lx.realestatemarket.core.api.IPropertyQueriesPort;
import pt.lx.realestatemarket.core.spi.IPropertiesRepositoryPort;
import pt.lx.realestatemarket.core.usecases.PropertyCommands;
import pt.lx.realestatemarket.core.usecases.PropertyQueries;

@Configuration
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class ApplicationConfiguration {

    @Autowired
    IPropertiesRepositoryPort propertyRepositoryPort;

    @Autowired
    IPropertyQueriesPort addPropertyServicePort;

    @Bean
    public IPropertyQueriesPort propertyQueriesPort() {
        return new PropertyQueries(propertyRepositoryPort);
    }

    @Bean
    public IPropertyCommandsPort propertyCommandsPort() {
        return new PropertyCommands(propertyRepositoryPort);
    }

}