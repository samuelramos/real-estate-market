package pt.lx.realestatemarket.core.usecases;

import pt.lx.realestatemarket.core.usecases.mapper.PropertyMapper;
import pt.lx.realestatemarket.domain.business.Property;
import pt.lx.realestatemarket.shared.api.dto.PropertyDto;
import pt.lx.realestatemarket.core.api.IPropertyCommandsPort;
import pt.lx.realestatemarket.core.spi.IPropertiesRepositoryPort;

public class PropertyCommands implements IPropertyCommandsPort {

    private PropertyMapper propertyMapper;
    private IPropertiesRepositoryPort propertiesRepositoryPort;

    public PropertyCommands(IPropertiesRepositoryPort propertiesRepositoryPort) {
        this.propertiesRepositoryPort = propertiesRepositoryPort;
        propertyMapper = new PropertyMapper();
    }

    @Override
    public Long saveProperty(PropertyDto propertyDto) {

        Property property = propertyMapper.api2domain(propertyDto);

        //triggers business logic execution

        Property result = propertyMapper.spi2domain(propertiesRepositoryPort.save(propertyMapper.domain2spi(property)));

        //triggers business logic execution

        return result.getId();
    }

    @Override
    public void deleteProperty(Long propertyId) {
        propertiesRepositoryPort.delete(propertyId);
    }

}
