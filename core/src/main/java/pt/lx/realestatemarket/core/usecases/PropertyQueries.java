package pt.lx.realestatemarket.core.usecases;

import pt.lx.realestatemarket.core.usecases.mapper.PropertyMapper;
import pt.lx.realestatemarket.domain.business.Property;
import pt.lx.realestatemarket.shared.api.dto.PropertyDto;
import pt.lx.realestatemarket.core.api.IPropertyQueriesPort;
import pt.lx.realestatemarket.core.spi.IPropertiesRepositoryPort;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public class PropertyQueries implements IPropertyQueriesPort {

    private PropertyMapper propertyMapper;
    private IPropertiesRepositoryPort propertiesRepositoryPort;

    public PropertyQueries(IPropertiesRepositoryPort propertiesRepositoryPort) {
        this.propertiesRepositoryPort = propertiesRepositoryPort;
        propertyMapper = new PropertyMapper();
    }

    @Override
    public List<PropertyDto> getAllProperties() {

        List <Property> result = propertiesRepositoryPort.getAll().stream().map(p -> propertyMapper.spi2domain(p)).collect(Collectors.toList());

        //triggers business logic execution

        return result.stream().map(p -> propertyMapper.domain2api(p)).collect(Collectors.toList());
    }

    @Override
    public Optional<PropertyDto> getPropertyById(Long propertyId) {

        Optional <Property> result = propertiesRepositoryPort.getById(propertyId).map(p -> propertyMapper.spi2domain(p));

        //triggers business logic execution

        return result.map(p -> propertyMapper.domain2api(p));
    }

}