package pt.lx.realestatemarket.core.usecases.mapper;

import lombok.NoArgsConstructor;
import pt.lx.realestatemarket.domain.business.Property;


@NoArgsConstructor
public class PropertyMapper {

    public pt.lx.realestatemarket.shared.spi.dto.PropertyDto domain2spi(Property property) {
        return new pt.lx.realestatemarket.shared.spi.dto.PropertyDto(property.getId(), property.getUrl(), property.getName());
    }

    public Property spi2domain(pt.lx.realestatemarket.shared.spi.dto.PropertyDto propertyDto) {
        return new Property(propertyDto.getId(), propertyDto.getUrl(), propertyDto.getName());
    }

    public pt.lx.realestatemarket.shared.api.dto.PropertyDto domain2api(Property property) {
        return new pt.lx.realestatemarket.shared.api.dto.PropertyDto(property.getId(), property.getUrl(), property.getName());
    }

    public Property api2domain(pt.lx.realestatemarket.shared.api.dto.PropertyDto propertyDto) {
        return new Property(propertyDto.getId(), propertyDto.getUrl(), propertyDto.getName());
    }

}
