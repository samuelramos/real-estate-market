package pt.lx.realestatemarket.core.spi;

import pt.lx.realestatemarket.shared.spi.dto.PropertyDto;
import java.util.List;
import java.util.Optional;


public interface IPropertiesRepositoryPort {

    List<PropertyDto> getAll();
    Optional<PropertyDto> getById(Long id);
    PropertyDto save(PropertyDto property);
    void delete(Long id);

}
