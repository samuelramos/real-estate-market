package pt.lx.realestatemarket.core.api;

import pt.lx.realestatemarket.shared.api.dto.PropertyDto;
import java.util.List;
import java.util.Optional;


public interface IPropertyQueriesPort {

    List<PropertyDto> getAllProperties();
    Optional<PropertyDto> getPropertyById(Long propertyId);

}
