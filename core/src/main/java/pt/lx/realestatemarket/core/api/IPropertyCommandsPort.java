package pt.lx.realestatemarket.core.api;

import pt.lx.realestatemarket.shared.api.dto.PropertyDto;


public interface IPropertyCommandsPort {

    Long saveProperty(PropertyDto property);
    void deleteProperty(Long propertyId);

}
