package pt.lx.realestatemarket.core.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pt.lx.realestatemarket.core.spi.IPropertiesRepositoryPort;
import pt.lx.realestatemarket.core.api.IPropertyCommandsPort;
import pt.lx.realestatemarket.core.usecases.PropertyCommands;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class AddPropertyV1Test {

    private IPropertiesRepositoryPort propertiesRepositoryPort = Mockito.mock(IPropertiesRepositoryPort.class);
    private IPropertyCommandsPort propertiesServicePort;

    @BeforeEach
    void initMocks() {
        propertiesServicePort = new PropertyCommands(propertiesRepositoryPort);
    }

    @Test
    public void scenarioA() {

        pt.lx.realestatemarket.shared.api.dto.PropertyDto property = new pt.lx.realestatemarket.shared.api.dto.PropertyDto(Long.parseLong("1"), "http://teste-1/", "test-1");
        when(propertiesRepositoryPort.save(any(pt.lx.realestatemarket.shared.spi.dto.PropertyDto.class))).then(returnsFirstArg());
        Long propertyId = propertiesServicePort.saveProperty(property);
        Assertions.assertEquals(property.getId(),propertyId);

    }

}
