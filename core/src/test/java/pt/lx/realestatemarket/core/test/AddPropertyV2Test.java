package pt.lx.realestatemarket.core.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pt.lx.realestatemarket.core.spi.IPropertiesRepositoryPort;
import pt.lx.realestatemarket.core.api.IPropertyCommandsPort;
import pt.lx.realestatemarket.core.usecases.PropertyCommands;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class AddPropertyV2Test {

    @Mock
    private IPropertiesRepositoryPort propertiesRepositoryPort;

    @InjectMocks
    private IPropertyCommandsPort propertiesServicePort = new PropertyCommands(propertiesRepositoryPort);

    @BeforeEach
    void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void scenarioA(){

        pt.lx.realestatemarket.shared.api.dto.PropertyDto property = new pt.lx.realestatemarket.shared.api.dto.PropertyDto(Long.parseLong("1"), "http://teste-1/", "test-1");
        when(propertiesRepositoryPort.save(any(pt.lx.realestatemarket.shared.spi.dto.PropertyDto.class))).then(returnsFirstArg());
        Long propertyId = propertiesServicePort.saveProperty(property);
        Assertions.assertEquals(property.getId(),propertyId);

    }

}
