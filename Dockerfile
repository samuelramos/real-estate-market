FROM openjdk:8-jdk-alpine

RUN addgroup -S spring && adduser -S spring -G spring

RUN mkdir -p /opt/real-estate-market \
    && chown spring:spring /opt/real-estate-market
RUN mkdir -p /var/log/real-estate-market \
    && chown spring:spring /var/log/real-estate-market

USER spring:spring

ARG APPJAR=application/target/*.jar
COPY ${APPJAR} /opt/real-estate-market/app.jar

ENTRYPOINT ["java","-jar","/opt/real-estate-market/app.jar"]
