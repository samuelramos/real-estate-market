package pt.lx.realestatemarket.domain.business;

import lombok.AllArgsConstructor;
import lombok.Data;


@AllArgsConstructor
@Data
public class Property {

    private Long id;
    private String url;
    private String name;

}
